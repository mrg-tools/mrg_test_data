from setuptools import setup, find_packages
# import os
# import sys
# print(os.path.dirname(os.path.abspath(__file__)))
# sys.path.append(os.path.dirname(os.path.abspath(__file__)))

# import versioneer
#
# setup(
#     version=versioneer.get_version(),
#     cmdclass=versioneer.get_cmdclass(),
#     packages=find_packages(),
#     package_data={"mrg_core": ["py.typed"]}
#     # data_files=get_data_files(),
# )


setup(package_data={"mrg_test_data": ["py.typed"]}, packages=find_packages())
