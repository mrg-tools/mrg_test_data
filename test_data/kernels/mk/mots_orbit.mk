  \begindata

       PATH_SYMBOLS    = ('KERNELS')
       PATH_VALUES     = ('/data/spice/generic')

       KERNELS_TO_LOAD = (
             '$KERNELS/fk/earth_assoc_itrf93.tf',
             '$KERNELS/lsk/naif0012.tls',
             '$KERNELS/pck/de-403-masses.tpc',
             '$KERNELS/pck/gm_de431.tpc',
             '$KERNELS/pck/pck00010.tpc',
             '$KERNELS/pck/earth_200101_990628_predict.bpc',
             '$KERNELS/pck/earth_720101_070426.bpc',
             '$KERNELS/spk/de432s.bsp'
                          )